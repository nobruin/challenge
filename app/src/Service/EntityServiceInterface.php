<?php

namespace App\Service;

interface EntityServiceInterface
{
    public function create(string $json);
}