<?php 

namespace App\EventListener;

use App\Helper\ResponseHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionListener implements EventSubscriberInterface
{
    public function __construct() {
        
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['handle404Exeception', 0],
                ['handle406Exeception', 1],
                ['handle400Exeception', 2]
                ]
        ];
    }


    public function handle404Exeception(ExceptionEvent $event){
        
        if(!$event->getException() instanceof NotFoundHttpException){
            return;
        }

        $response = $this->formatResponse($event->getException(), Response::HTTP_NOT_FOUND);
        $event->setResponse($response);
    }

    public function handle406Exeception(ExceptionEvent $event)
    {
        if(!$event->getException() instanceof NotAcceptableHttpException){
            return;
        }
        
        $response = $this->formatResponse($event->getException(), Response::HTTP_NOT_ACCEPTABLE);
        $event->setResponse($response);
    }

    public function handle400Exeception(ExceptionEvent $event)
    {
        if(!$event->getException() instanceof BadRequestHttpException){
            return;
        }
        
        $response = $this->formatResponse($event->getException(), Response::HTTP_BAD_REQUEST);
        $event->setResponse($response);
    }    

    public function formatResponse(\Throwable $execption,int $statusCode)
    {
        $response = ResponseHelper::fromError($execption)->getExecptionResponse();
        $response->setStatusCode($statusCode);

        return $response;
    }
}
